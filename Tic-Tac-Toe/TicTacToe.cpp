#include "TicTacToe.h"

TicTacToe::TicTacToe(QWidget *parent) : QWidget(parent), currentPlayer('X'), moves(0)
{
    QGridLayout* gridLayout = new QGridLayout(this);
    setLayout(gridLayout);
    gridLayout->setSpacing(1);

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            QPushButton* button = new QPushButton(this);
            button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
            button->setFont(QFont("Arial", 30));
            gridLayout->addWidget(button, i, j);
            connect(button, &QPushButton::clicked, this, [this, button]() { HandleButtonClick(button); });
            buttons[i][j] = button;
        }
    }
}

void TicTacToe::HandleButtonClick(QPushButton* button)
{
    button->setText(QString(currentPlayer));

    if (CheckWinCondition())
    {
        QMessageBox::information(this, "Game Over!", QString("%1 wins!").arg(currentPlayer));
        RestartGame();
    }
    else if (moves == 8)
    {
        QMessageBox::information(this, "Game Over!", "Its a draw!");
        RestartGame();
    }
    else
    {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
        moves++;
    }
}
bool TicTacToe::CheckWinCondition()
{
    const QString symbol = QString(currentPlayer);

    for (int i = 0; i < 3; ++i)
    {
        if (buttons[i][0]->text() == symbol && buttons[i][1]->text() == symbol && buttons[i][2]->text() == symbol)
            return true;

        if (buttons[0][i]->text() == symbol && buttons[1][i]->text() == symbol && buttons[2][i]->text() == symbol)
            return true;
    }

    if (buttons[0][0]->text() == symbol && buttons[1][1]->text() == symbol && buttons[2][2]->text() == symbol)
        return true;

    if (buttons[0][2]->text() == symbol && buttons[1][1]->text() == symbol && buttons[2][0]->text() == symbol)
        return true;

    return false;
}
void TicTacToe::RestartGame()
{
    currentPlayer = 'X';
    moves = 0;

    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            buttons[i][j]->setText("");
}