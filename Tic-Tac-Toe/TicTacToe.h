#pragma once

#include <QtWidgets/QWidget>
#include "ui_TicTacToe.h"
#include <QMessageBox>
#include <QPushButton>
#include <QGridLayout>

class TicTacToe : public QWidget
{
    Q_OBJECT

public:
    TicTacToe(QWidget* parent = nullptr);
private:
    QPushButton* buttons[3][3];
    char currentPlayer;
    int moves;

    bool CheckWinCondition();
    void RestartGame();

private slots:
    void HandleButtonClick(QPushButton* button);
};
