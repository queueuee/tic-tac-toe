/********************************************************************************
** Form generated from reading UI file 'TicTacToe.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TICTACTOE_H
#define UI_TICTACTOE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TicTacToeClass
{
public:

    void setupUi(QWidget *TicTacToeClass)
    {
        if (TicTacToeClass->objectName().isEmpty())
            TicTacToeClass->setObjectName(QString::fromUtf8("TicTacToeClass"));
        TicTacToeClass->resize(600, 400);

        retranslateUi(TicTacToeClass);

        QMetaObject::connectSlotsByName(TicTacToeClass);
    } // setupUi

    void retranslateUi(QWidget *TicTacToeClass)
    {
        TicTacToeClass->setWindowTitle(QCoreApplication::translate("TicTacToeClass", "TicTacToe", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TicTacToeClass: public Ui_TicTacToeClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TICTACTOE_H
